import argparse
import java_to_struct as jts
import struct_to_html as sth
import os


def arg_parser() -> argparse.Namespace:
    description = "Compiles Java Code to HTML Documentation"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('name', type=str,
                        help="Directory with Java project or Java file")
    parser.add_argument("--out", default=None,
                        help="Path to the output documentation")
    parser.add_argument('--encodings', nargs='+', default=['utf-8'],
                        help='Encodings for reading java files')
    parser.add_argument('--console', action='store_true',
                        help='Print result to the console,'
                             ' only for single files available')
    parser.add_argument("--content", action="store_true",
                        help="Create table of contents")
    return parser.parse_args()


def make_doc(file_name: str, path: str, home_dir: str, encodings: list):
    file_sub_path = path[len(home_dir):]
    struct = jts.get_struct(f'{path}/{file_name}', encodings)
    struct.reverse()
    out_file = args.out + f'{file_sub_path}/' \
                          f'{os.path.splitext(file_name)[0]}.html'
    os.makedirs(args.out + file_sub_path, exist_ok=True)

    with open(out_file, 'w') as f:
        f.write(f"<!DOCTYPE html>\n<html>\n<h1>{file_name}</h1>")
        sth.write(struct, f)

    if args.content:
        with open(args.out + "/Content.html", "a") as f:
            f.write(f"<li><a href={out_file}>"
                    f"{file_sub_path}/{file_name}</a></li>")


def directory_mode(args: argparse.Namespace):
    tree = os.walk(args.name)
    try:
        os.makedirs(args.out)
    except FileExistsError:
        pass

    if args.content:
        with open(args.out + "/Content.html", "w") as f:
            f.write(f"<!DOCTYPE html>\n<html>\n<h1>Content</h1><ul>")

    for path, folders, files in tree:
        for file in files:
            if is_java_file(file):
                make_doc(file, path, args.name, args.encodings)

    if args.content:
        with open(args.out + "/Content.html", "a") as f:
            f.write("</ul>")


def console_mode(full_file_name: str, encodings: list):
    print(full_file_name)
    print()
    file_name = full_file_name[full_file_name.rfind('/') + 1::]
    struct = jts.get_struct(full_file_name, encodings)
    struct.reverse()
    for space in struct:
        print(file_name + str(space))


def is_java_file(path: str):
    return os.path.splitext(path)[1] == '.java'


if __name__ == '__main__':
    args = arg_parser()
    args.name = os.path.abspath(args.name)
    if not args.out:
        args.out = args.name
    args.out = os.path.abspath(args.out)
    if args.console and is_java_file(args.name):
        console_mode(args.name, args.encodings)
    elif args.console and not is_java_file(args.name):
        print("Console mode can be use only for single file, not for project")
    elif os.path.isdir(args.name):
        directory_mode(args)
    elif is_java_file(args.name):
        path, name = os.path.split(args.name)
        make_doc(name, path, path, args.encodings)
    else:
        print("Wrong File")
