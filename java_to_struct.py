import re
import itertools
import typing

ACCESS_MODES = {'private', 'public', 'protected', 'package-private'}
DESCRIPTOR_CLASS = {'@author', '@version', '@since', '@see', '@deprecated',
                    '@link'}
DESCRIPTOR_METHOD = {'@since', '@see', '@param', '@return', '@exception',
                     '@deprecated', '@link'}


class Class:
    """Хранит информацию для классов и интерфейсов"""
    def __init__(self, name: str, doc: str):
        self.name = ''
        self.type = ''
        self.access_mode = ''
        self.extends = None
        self.implements = None
        self.docx = (re.sub(r'\s+', ' ', doc, flags=re.MULTILINE).split('*')
                     if doc else None)
        self.parse_name(name)
        self.javadoc = parse_doc(self.docx)

    def __eq__(self, other):
        return self.name == other.name and self.docx == other.docx

    def __str__(self):
        docx = "\n*".join(self.docx) if self.docx else None
        impl = " ".join(self.implements) if self.implements else None
        return (f'{self.access_mode} {self.type} {self.name}\n'
                + f'extends {self.extends}\n'
                + f'implements {impl}\n'
                + f'Documentation:{docx}\n')

    def parse_name(self, name):
        name = re.sub(r'\s+', ' ', name, flags=re.MULTILINE).strip().split()
        self.access_mode = (name[0]
                            if name[0] in ACCESS_MODES else 'package-private')
        if 'implements' in name:
            self.implements = []
            for i in name[name.index("implements") + 1:]:
                if i == "extends":
                    break
                self.implements.append(i)

        self.extends = (name[name.index('extends') + 1]
                        if 'extends' in name else None)
        self.type = 'class' if 'class' in name else 'interface'
        self.name = name[name.index(self.type) + 1]


def parse_doc(documentation: typing.List[str]) -> typing.Dict[str, str]:
    if not documentation:
        return dict()
    described_doc = {'regular': ''}

    for line in documentation:
        line = line.strip()
        if line.startswith('@'):
            key, value = line.split(' ', 1)
            if not key in described_doc:
                described_doc[key] = []
            described_doc[key].append(value)
            continue
        elif re.search(r'{@link (.+?)}', line):
            if not "@link" in described_doc:
                described_doc["@link"] = []
            described_doc['@link'].append(re.search(r'{@link (?P<link>.+?)}',
                                                    line).group('link'))
        described_doc['regular'] += line
    return described_doc


class Function:
    """Хранит информацию о функциях"""
    def __init__(self, name: str, signature: str, doc: str):
        self.name = ''
        self.decorator = None
        self.signature = re.sub(r'\s+', ' ', signature, flags=re.MULTILINE)
        self.docx = (re.sub(r'\s+', ' ', doc, flags=re.MULTILINE).split('*')
                     if doc else None)
        self.access_mode = ''
        self.return_type = None
        self.parse_name(name)
        self.javadoc = parse_doc(self.docx)

    def __eq__(self, other):
        return (self.name == other.name
                and self.signature == other.signature
                and self.docx == other.docx
                and self.access_mode == other.access_mode
                and self.return_type == other.return_type)

    def __str__(self):
        docx = "\n*".join(self.docx)
        return (f'{self.access_mode} {self.name}'
                + f'{self.signature} -> {self.return_type}\n'
                + f'Documentation:{docx}')

    def parse_name(self, name: str):
        name = re.sub(r'\s+', ' ', name, flags=re.MULTILINE).strip().split()
        if name[0].startswith('@'):
            self.decorator = name[0]
            name = name[1:]
        if name[0] in ACCESS_MODES:
            self.access_mode = name[0]
            name = name[1:]
        else:
            self.access_mode = 'package-private'
        self.name = name[-1]
        self.return_type = (' '.join(itertools.dropwhile(
            lambda x: x in ACCESS_MODES or x == 'static',
            name[:-1])) if len(name) > 1 else 'constructor')


class Space:
    def __init__(self, parent, namespace: typing.Union[Class, Function],
                 start: int, end: int):
        self.namespace = namespace
        self.parent = parent
        self.children = []
        self.start = start
        self.end = end
        self.last_index = start
        if parent is None:
            self.depth = 1
        else:
            self.depth = parent.depth + 1

    def __str__(self):
        path = []
        space = self.parent
        while space:
            path.append(space.namespace.name)
            space = space.parent
        path.reverse()
        path = "/" + '/'.join(path)
        return f'{path}\n{str(self.namespace)}\n'

    def __eq__(self, other):
        return (self.parent == other.parent
                and self.namespace == other.namespace
                and self.children == other.children
                and self.start == other.start
                and self.end == other.end)


class Parser:
    def __init__(self, text: str):
        self.result: typing.List[Space] = []
        self.index = 0
        self.pattern_name = (r'^\s*(?P<name>[^;=(*/)\(\)\{\}]+?)'
                             r'(?P<signature>\([^;\*\{\}]*?\)){0,1}'
                             r'\s*(?P<exception>throws \w+){0,1}\s*{')
        self.pattern_doc = r'/\*\*(.*?)\*/'
        self.text = text
        self.blocks = dict()
        self.automaton()

    # Мы пока не смотрим на св-ва, только фигурные скобочки, циклы игнорим
    def get_new_space(self, cur_space: Space) -> Space:
        start, end = self.get_bounds(cur_space)
        if start == -1:
            self.result.append(cur_space)
            return cur_space.parent
        try:
            new_doc = self.get_doc(cur_space, start)
            new_namespace = self.get_name(cur_space)
            if new_namespace.group('signature'):
                new_namespace = Function(new_namespace.group('name'),
                                         new_namespace.group('signature'),
                                         new_doc)
            else:
                new_namespace = Class(new_namespace.group('name'), new_doc)
        except Exception:
            cur_space.last_index = end
            return cur_space

        new_space = Space(cur_space, new_namespace, start, end)
        cur_space.children.append(new_space)
        cur_space.last_index = end
        if isinstance(new_namespace, Class):
            return new_space
        self.result.append(new_space)
        return cur_space

    def get_name(self, cur_space: Space):
        new_namespace = re.search(self.pattern_name,
                                  self.text[cur_space.last_index::],
                                  re.DOTALL | re.MULTILINE)
        if not new_namespace:
            raise Exception("Name not found")
        return new_namespace

    def get_doc(self, cur_space: Space, end: int) -> str:
        new_docs = re.findall(self.pattern_doc,
                              self.text[cur_space.last_index:end:],
                              re.DOTALL | re.MULTILINE)
        return new_docs[-1] if new_docs else None

    def get_bounds(self, cur_space: Space, start_index=None) -> tuple:
        if start_index is None:
            start_index = cur_space.last_index + 1
        start = self.text[start_index:cur_space.end:].find('{')
        if start == -1:
            return -1, -1
        start += start_index
        try:
            end = self.blocks[start]
        except KeyError:
            return self.get_bounds(cur_space, start + 1)
        return start, end

    def start(self):
        # Ищем главный класс
        start = self.text.find('{')
        if start == -1:
            return
        while start not in self.blocks:
            if self.text[start+1:].find('{') == -1:
                return
            start = self.text[start+1:].find('{') + start + 1

        end = self.blocks[start]

        name = re.search(self.pattern_name, self.text,
                         re.DOTALL | re.MULTILINE)
        docs = re.findall(self.pattern_doc, self.text[:start:],
                          re.DOTALL | re.MULTILINE)
        doc = docs[-1] if docs else None
        try:
            namespace = Class(name.group('name'), doc)
            cur_space = Space(None, namespace, start, end)
        except ValueError:
            namespace = Function(name.group('name'), "", doc)
            cur_space = Space(None, namespace, start, end)
            self.result.append(cur_space)
            return

        # Делаем обход в глубину
        while cur_space is not None:
            cur_space = self.get_new_space(cur_space)

    # Принимает индекс начала бесполезной инфы - строка или коммент
    def del_trash(self):
        i = self.index
        sym = self.text[self.index]

        if self.text[self.index:self.index + 2:] == '/*':
            while self.text[i - 2:i:] != '*/':
                i += 1

        elif self.text[self.index:self.index + 2:] == '//':
            while self.text[i] != '\n':
                i += 1

        elif sym == '"' or sym == "'":
            i += 1
            while self.text[i] != sym:
                if self.text[i] == '\\':
                    i += 2
                    continue
                i += 1
            i += 1
        else:
            self.index += 1
            return

        self.text = self.text[:self.index] + self.text[i:]

    def skip_doc(self):
        while self.text[self.index - 2:self.index:] != '*/':
            self.index += 1

    # Все комментарии и строки удаляются, доки пропускаются
    # создается словарь областей видимости, включая внутренности ф-ий,
    # они потом будут выброшены при классификации имен
    # результат - чистый текст и в self.blocks лежит словарь над новым текстом
    def automaton(self):
        opened = []
        blocks = dict()
        while self.index < len(self.text):
            sym = self.text[self.index]
            if sym == '{':
                opened.append(self.index)
                self.index += 1
            elif sym == '}':
                blocks[self.index] = opened[-1]
                blocks[opened.pop()] = self.index
                self.index += 1
            elif self.text[self.index:self.index + 3] == '/**':
                self.skip_doc()
            elif sym in {'/', '"', "'"}:
                self.del_trash()
            else:
                self.index += 1
        self.blocks = blocks


def get_struct(file_name, encodings) -> list:
    with open(file_name, encoding=encodings[0]) as f:
        p = Parser(f.read())
    p.start()

    return p.result
