# python_task_javadoc

Javadoc
========
Now we can parse namespases and save in new structure
for further processing 
As a result you will get output with next structure:
> class ClassName
>
> Class documentation
>
Where count of tabs - current structure depth,
the first line is the class name, the second is the
documentation
 
How to Run
==========
For more actual information:
>javadoc.py -h
>
Simple example: 
>javadoc.py ./examples/1.java --out ./doc.html
