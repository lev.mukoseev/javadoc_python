import java_to_struct as jts
import typing


def make_tok(spaces: typing.List[jts.Space], file):
    counter = 1
    for space in spaces:
        file.write("<br>"
                   + '-->' * (space.depth - 1)
                   + f"<a href='#p{counter}'>{space.namespace.name} "
                   + (space.namespace.signature
                      if isinstance(space.namespace, jts.Function) else '')
                   + '</a>')
        counter += 1


def link_checker(link: str) -> str:
    # оформлена web ссылка
    if link.startswith('<a href='):
        return link
    # ссылка на класс в текущем пакете
    link = link.split('#', 1)[0]
    return f'<a href=".\\{link}.html">' \
           f'{link.replace("#", ".")}</a>'


def get_name_html(namespace: typing.Union[jts.Class, jts.Function]):
    if isinstance(namespace, jts.Class):
        return (f'<span style="background-color: #AAAAAA"><u>'
                + f'{namespace.access_mode} {namespace.type}'
                + f' <b>{namespace.name}</b></u>'
                + ("<br>extends " + namespace.extends
                   if namespace.extends else "")
                + ("<br>implements " + " ".join(
                    namespace.implements) if namespace.implements else "")
                + f'</span>')
    return (f'<span style="background-color: #AAAAAA">'
            + f'{namespace.decorator + "<br>" if namespace.decorator else ""}'
            + f'{namespace.access_mode} <b>{namespace.name}</b>'
            + f'{namespace.signature} -> {namespace.return_type}</span>')


def get_doc_html(docs: typing.List[str]):
    if not docs:
        return ""
    doc = '<br>'.join(docs)
    if len(docs) == 1:
        doc = '<br>' + doc
    return f'<span style="background-color: #CCCCCC">{doc}</span>'


def get_javadoc_html(docs: typing.Union[typing.Dict[str, list],
                                        typing.Dict[str, str]]):
    if not docs:
        return ""
    doc = '<br>' + docs['regular']
    for descriptor in docs:
        if descriptor == 'regular':
            continue
        if descriptor in {'@link', '@see'}:
            for line in docs[descriptor]:
                line = line.strip()
                doc += f'<br><b>{descriptor}:</b>\t{link_checker(line)}'
            continue
        if descriptor == '@param':
            doc += '<br><b>params:</b>'
            for line in docs[descriptor]:
                line = line.strip()
                x = line.split(' ')
                doc += f'<br>>\t<b>{x[0]}</b> {" ".join(x[1:])}'
            continue
        doc += f'<br><b>{descriptor}:</b>\t{" ".join(docs[descriptor])}'
    return f'<span style="background-color: #CCCCCC">{doc}</span>'


DOC_MODES = {
    'javadoc': lambda space: get_javadoc_html(space.namespace.javadoc),
    'regular': lambda space: get_doc_html(space.namespace.docx)
}


def write(spaces: typing.List[jts.Space], file, mode='javadoc'):
    make_tok(spaces, file)
    counter = 1
    for space in spaces:
        file.write('<blockquote>' * (space.depth - 1)
                   + f'<p id="p{counter}"></p>'
                   + get_name_html(space.namespace)
                   + DOC_MODES[mode](space)
                   + '</blockquote>' * (space.depth - 1))
        counter += 1
