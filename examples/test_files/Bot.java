import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

/** Телеграм Бот */
public class Bot extends TelegramLongPollingBot {
    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try{
            telegramBotsApi.registerBot(new Bot());

        } catch(TelegramApiException e){
            e.printStackTrace();
        }

    }

    /**
     * Функция обработки сообщения
    */
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        if(message != null && message.hasText()){
            String text = message.getText();

            if (text.equals("/help"))
            {
                var response = "Я бот, который поможет тебе вспомнить, когда и какие у тебя пары и подскажет, какая на улице погода\n" +
                               "/info Покажет тебе расписание и погоду на текущий день \n" +
                               "/schedule Выдаст расписание на следующие три дня \n" +
                               "/weather Расскажет о погоде на ближайшую неделю \n" +
                               "/changeInfo Установит твою группу в вузе и место жительства или поменяет уже имеющуюся информацию";
                sendMsg(message, response, true);
            }
            else if (text.equals("/changeInfo"))
            {
                sendMsg(message, "Что поменяем?", true);
            }
            else if (text.matches("/schedule .*")) {
                sendMsg(message, Schedule.getSchedule(text), false);
            }
            else
            {
                sendMsg(message, "Извини, но я тебя не понял, попробуй еще раз или напиши /help");
            }
        }
    }


    private void sendMsg(Message message, String text, boolean isReply) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        if (isReply)
            sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendMsg(Message message, String text){
        sendMsg(message, text, false);
    }

    public String getBotUsername() {
        return "java_project";
    }

    public String getBotToken() {
        return "815206226:AAGdEge7flJz5L1JKUM2t8X3DoQloKFWsro";
    }
}
