import unittest
from java_to_struct import Parser, Space, Class, Function


class TestSubFunc(unittest.TestCase):
    MAINCLASS = Space(None, Class('class SubProduct', '\n * Бесполезный \n'),
                      40, 347)

    def test_bounds_first(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = self.MAINCLASS.start
            self.assertEqual(Parser(f.read()).get_bounds(self.MAINCLASS),
                             (131, 166))

    def test_bounds_second(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = 166
            self.assertEqual(Parser(f.read()).get_bounds(self.MAINCLASS),
                             (211, 238))

    def test_name_func(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = self.MAINCLASS.start
            self.assertEqual(
                Parser(f.read()).get_name(self.MAINCLASS).group('name'),
                'public static int Get')

    def test_name_class(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = 238
            self.assertEqual(
                Parser(f.read()).get_name(self.MAINCLASS).group('name'),
                'class Another')

    def test_get_blocks(self):
        self.assertEqual(Parser('cds {{}}').blocks, {4: 7, 5: 6, 7: 4, 6: 5})

    def test_get_blocks_empty(self):
        self.assertEqual(Parser('').blocks, dict())

    def test_doc(self):
        with open('./tests/test1.java', encoding='utf-8') as f:
            self.MAINCLASS.last_index = self.MAINCLASS.start
            self.assertEqual('\n     * Еще более бесполезгая ф-ия\n    ',
                             Parser(f.read()).get_doc(self.MAINCLASS, 132))

    def test_start(self):
        with open('./tests/test2.java', encoding='utf-8') as f:
            parser = Parser(f.read())
            parser.start()
            expected = Space(None, Class('public class Set',
                                         '\n * Что-то про set\n'), 42, 45)
            self.assertTrue(parser.result[0] == expected)

    def test_fake_method(self):
        with open('./tests/fake_method.java', encoding='utf-8') as f:
            parser = Parser(f.read())
            parser.start()
            self.assertEqual([], parser.result[0].children)

    def test_ml_signature(self):
        with open('./tests/ml_signature.java', encoding='utf-8') as f:
            parser = Parser(f.read())
            parser.start()
            method = parser.result[0].namespace
            self.assertEqual('B', method.name)
            self.assertEqual('(int a1, int a2, int a3)', method.signature)
            self.assertEqual('int', method.return_type)
            self.assertEqual('public', method.access_mode)

    def test_interface(self):
        with open('./tests/interface.java', encoding='utf-8') as f:
            parser = Parser(f.read())
            parser.start()
            self.assertEqual([], parser.result[0].children)
            self.assertEqual('interface', parser.result[0].namespace.type)
            self.assertEqual('A', parser.result[0].namespace.name)
            self.assertEqual('public', parser.result[0].namespace.access_mode)
            self.assertEqual([' doc '], parser.result[0].namespace.docx)


class TestAutomaton(unittest.TestCase):
    EXPECTED_COMMENT = "\nclass Product\n{\n\n    public int count;\n}"

    def test_multiline_comment(self):
        with open('./tests/multiline_comment.java', encoding='utf-8') as f:
            result = Parser(f.read()).text
            self.assertEqual(self.EXPECTED_COMMENT, result)

    def test_single_line_comment(self):
        with open('./tests/single_line_comment.java', encoding='utf-8') as f:
            result = Parser(f.read()).text
            self.assertEqual(self.EXPECTED_COMMENT, result)

    def test_all_comment(self):
        with open('./tests/all_comment.java', encoding='utf-8') as f:
            result = Parser(f.read()).text
            self.assertEqual(self.EXPECTED_COMMENT, result)

    def test_comment_in_doc(self):
        with open('./tests/comment_in_doc.java', encoding='utf-8') as f:
            text = f.read()
            result = Parser(text).text
            self.assertEqual(text, result)

    def test_doc_in_comment(self):
        with open('./tests/doc_in_comment.java', encoding='utf-8') as f:
            text = f.read()
            result = Parser(text).text
            self.assertEqual(self.EXPECTED_COMMENT, result)

    def test_string(self):
        with open('./tests/strings.java', encoding='utf-8') as f:
            result = Parser(f.read()).text
            self.assertEqual(self.EXPECTED_COMMENT, result)

    def test_blocks_standard(self):
        result = Parser("{ 123456{78}9 }").blocks
        self.assertEqual({0: 14, 14: 0, 8: 11, 11: 8}, result)

    def test_blocks_with_trash(self):
        result = Parser("{'trash'}").blocks
        self.assertEqual({0: 1, 1: 0}, result)

    def test_code_in_comment(self):
        with open('./tests/code_in_comment.java', encoding='utf-8') as f:
            result = Parser(f.read()).text
            self.assertEqual("\n\n\n\nclass Product\n{\n    int count;\n}",
                             result)

    def test_code_in_comment_ml(self):
        with open('./tests/code_in_comment_ml.java', encoding='utf-8') as f:
            result = Parser(f.read()).text
            self.assertEqual("\nclass Product\n{\n    int count;\n}", result)

